#pragma once

#include <JuceHeader.h>
const double semi_map[] = {
        1.0,
        1.05946309436,
        1.12246204831,
        1.189207115,
        1.25992104989,
        1.33483985417,
        1.41421356237,
        1.49830707688,
        1.58740105197,
        1.68179283051,
        1.78179743628,
        1.88774862536,
        2.0
};
const juce::String str_map[] = {
        "P1",
        "m2",
        "M2",
        "m3",
        "M3",
        "P4",
        "d5",
        "P5",
        "m6",
        "M6",
        "m7",
        "M7",
        "P8"
};
//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent  : public juce::AudioAppComponent
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent() override;

    //==============================================================================
    void prepareToPlay (int samplesPerBlockExpected, double sampleRate) override;
    void getNextAudioBlock (const juce::AudioSourceChannelInfo& bufferToFill) override;
    void releaseResources() override;

    inline void updateAngleDelta1()
    {
        auto cyclesPerSample = currentFrequency1 / currentSampleRate;
        angleDelta1 = cyclesPerSample * 2.0 * juce::MathConstants<double>::pi;
    }

    inline void updateAngleDelta2()
    {
        auto cyclesPerSample = currentFrequency2 / currentSampleRate;
        angleDelta2 = cyclesPerSample * 2.0 * juce::MathConstants<double>::pi;
    }

    void updateF2(){

        int index = interval.getValue();
        bool stepByHz = step.getToggleState();

        if(stepByHz) {

            interval.setRange (0, 100, 1);
            targetFrequency2 = index + targetFrequency1;
            intv.setText("IL    --", juce::dontSendNotification);
        } else {

            interval.setRange (0, 12, 1);
            targetFrequency2 = semi_map[index] * targetFrequency1;
            intv.setText("IL    " + str_map[index], juce::dontSendNotification);
        }

        f1.setText("F1   " + juce::String(targetFrequency1, 1) + " Hz", juce::dontSendNotification);
        f2.setText("F2   " + juce::String(targetFrequency2, 1) + " Hz" , juce::dontSendNotification);
        diff.setText("dF   " + juce::String((targetFrequency2 - targetFrequency1), 1) + " Hz" , juce::dontSendNotification);
    }
    //==============================================================================
    void paint (juce::Graphics& g) override;
    void resized() override;

private:
    //==============================================================================
    // Your private member variables go here...
    juce::Slider frequencySlider1;
    juce::Slider interval;

    juce::Label f1;
    juce::Label f2;
    juce::Label intv;
    juce::Label diff;

    juce::Label base;
    juce::Label increment;

    juce::ToggleButton mode;
    juce::ToggleButton step;
    juce::TextButton btn;

    double currentSampleRate = 0.0;
    double currentAngle1 = 0.0;
    double angleDelta1 = 0.0;

    double currentAngle2 = 0.0;
    double angleDelta2 = 0.0;

    double currentFrequency1 = 311.0;
    double targetFrequency1 = 311.0;
    double currentFrequency2 = 200.0;
    double targetFrequency2 = 200.0;

    float currentLevel = 0.1f, targetLevel = 0.1f;

    bool showOpts = false;
    juce::AudioDeviceSelectorComponent adsc;
    juce::OpenGLContext openGLContext;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};
