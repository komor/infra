#include "MainComponent.h"

//==============================================================================
MainComponent::MainComponent() : adsc(deviceManager, 2, 2, 2, 2, false, false, false, false)
{
    // Make sure you set the size of the component after
    // you add any child components.
    setSize (800, 600);

    // Some platforms require permissions to open input channels so request that here
    if (juce::RuntimePermissions::isRequired (juce::RuntimePermissions::recordAudio)
        && ! juce::RuntimePermissions::isGranted (juce::RuntimePermissions::recordAudio))
    {
        juce::RuntimePermissions::request (juce::RuntimePermissions::recordAudio,
                                           [&] (bool granted) { setAudioChannels (granted ? 2 : 0, 2); });
    }
    else
    {
        // Specify the number of input and output channels that we want to open
        setAudioChannels (2, 2);
    }

    auto devSetup = deviceManager.getAudioDeviceSetup();
    devSetup.bufferSize = 2000;
    deviceManager.setAudioDeviceSetup(devSetup, true);

    openGLContext.attachTo (*getTopLevelComponent());
    addAndMakeVisible (frequencySlider1);
    frequencySlider1.setSliderStyle(juce::Slider::SliderStyle::LinearBarVertical);
    frequencySlider1.setTextBoxStyle(juce::Slider::NoTextBox, true, 0, 0);
    frequencySlider1.setNumDecimalPlacesToDisplay(2);
    frequencySlider1.setVelocityBasedMode(true);
    frequencySlider1.setVelocityModeParameters(3.0);

    frequencySlider1.setRange (20.0, 400.0);
    //frequencySlider1.setSkewFactorFromMidPoint (300.0); // [4]
    frequencySlider1.setValue (currentFrequency1, juce::dontSendNotification);  // [6]
    frequencySlider1.onValueChange = [this] { targetFrequency1 = frequencySlider1.getValue(); updateF2();};


    interval.setSliderStyle(juce::Slider::SliderStyle::IncDecButtons);
   // interval.setNumDecimalPlacesToDisplay(0);
    interval.setRange (0, 12, 1);
    interval.setTextBoxStyle(juce::Slider::NoTextBox, false, 0, 0);
    //interval.setSkewFactorFromMidPoint (0.0); // [4]
    interval.setValue (0, juce::dontSendNotification);  // [6]
    interval.onValueChange = [this] { updateF2(); };
/*
    auto* panel = new juce::PropertyPanel("asdasd");

    juce::Array<juce::PropertyComponent*> props;
    props.add()
    panel->addProperties(props);
    concertinaPanel.addPanel(-1, panel, true);
    concertinaPanel.setMaximumPanelSize(panel, panel->getTotalContentHeight());
*/

    btn.setButtonText("=");
    btn.onClick = [this] { showOpts = !showOpts; resized();};
    btn.setColour(juce::TextButton::ColourIds::buttonColourId, getLookAndFeel().findColour(juce::Slider::ColourIds::trackColourId));

    mode.setButtonText(" Binaural");
    step.setButtonText(" Step by Hz");
    base.setText("BASE FREQ", juce::dontSendNotification);
    base.setJustificationType(juce::Justification::centred);

    increment.setText("INCREMENT", juce::dontSendNotification);
    increment.setJustificationType(juce::Justification::centred);
    step.onStateChange = [this] { updateF2();};

    addAndMakeVisible (interval);
    addAndMakeVisible(f1);
    addAndMakeVisible(f2);
    addAndMakeVisible(diff);
    addAndMakeVisible(intv);
    addAndMakeVisible(btn);
    addAndMakeVisible(adsc);
    addAndMakeVisible(mode);

    addAndMakeVisible(step);
    addAndMakeVisible(base);
    addAndMakeVisible(increment);
    //setLookAndFeel(new juce::LookAndFeel_V4(juce::LookAndFeel_V4::getGreyColourScheme()));
 //   addAndMakeVisible(concertinaPanel);

}

MainComponent::~MainComponent()
{
    shutdownAudio();
}

//==============================================================================
void MainComponent::prepareToPlay (int samplesPerBlockExpected, double sampleRate)
{

    currentSampleRate = sampleRate;
    updateF2();
    updateAngleDelta1();
    updateAngleDelta2();
}

void MainComponent::getNextAudioBlock (const juce::AudioSourceChannelInfo& bufferToFill)
{
    auto* leftBuffer  = bufferToFill.buffer->getWritePointer (0, bufferToFill.startSample);
    auto* rightBuffer = bufferToFill.buffer->getWritePointer (1, bufferToFill.startSample);

    bool bin = mode.getToggleState();
    // left
    auto localTargetFrequency = targetFrequency1;
    if (targetFrequency1 != currentFrequency1)
    {
        auto frequencyIncrement = (targetFrequency1 - currentFrequency1) / bufferToFill.numSamples;

        for (auto sample = 0; sample < bufferToFill.numSamples; ++sample)
        {
            auto currentSample = (float) std::sin (currentAngle1);
            currentFrequency1 += frequencyIncrement;
            updateAngleDelta1();
            currentAngle1 += angleDelta1;
            leftBuffer[sample]  = currentSample;

            if(!bin)
                rightBuffer[sample] = currentSample;
        }

        currentFrequency1 = localTargetFrequency;
    }
    else
    {
        for (auto sample = 0; sample < bufferToFill.numSamples; ++sample)
        {
            auto currentSample = (float) std::sin (currentAngle1);
            currentAngle1 += angleDelta1;
            leftBuffer[sample]  = currentSample;

            if(!bin)
                rightBuffer[sample] = currentSample;
        }
    }

    // right
    localTargetFrequency = targetFrequency2;
    if (targetFrequency2 != currentFrequency2)
    {
        auto frequencyIncrement = (targetFrequency2 - currentFrequency2) / bufferToFill.numSamples;

        for (auto sample = 0; sample < bufferToFill.numSamples; ++sample)
        {
            auto currentSample = (float) std::sin (currentAngle2);
            currentFrequency2 += frequencyIncrement;
            updateAngleDelta2();
            currentAngle2 += angleDelta2;

            if(bin) {
             //   leftBuffer[sample] += currentSample;
                rightBuffer[sample] = currentSample;

            } else {
                leftBuffer[sample] += currentSample;
                rightBuffer[sample] += currentSample;
            }
        }

        currentFrequency2 = localTargetFrequency;
    }
    else
    {
        for (auto sample = 0; sample < bufferToFill.numSamples; ++sample)
        {
            auto currentSample = (float) std::sin (currentAngle2);
            currentAngle2 += angleDelta2;

            if(bin) {
           //     leftBuffer[sample] += currentSample;
                rightBuffer[sample] = currentSample;
            } else {
                leftBuffer[sample] += currentSample;
                rightBuffer[sample] += currentSample;
            }
        }
    }

    auto localTargetLevel = targetLevel;
    bufferToFill.buffer->applyGainRamp (bufferToFill.startSample, bufferToFill.numSamples, currentLevel, localTargetLevel);
    currentLevel = localTargetLevel;
}

void MainComponent::releaseResources()
{
    // This will be called when the audio device stops, or when it is being
    // restarted due to a setting change.

    // For more details, see the help for AudioProcessor::releaseResources()
}

//==============================================================================
void MainComponent::paint (juce::Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)


    g.fillAll (getLookAndFeel().findColour (juce::ResizableWindow::backgroundColourId));
    auto r = getLocalBounds();
    g.setColour(getLookAndFeel().findColour(juce::Slider::ColourIds::trackColourId));
    g.fillRect(r.removeFromTop(90));
    // You can add your drawing code here!
}

void MainComponent::resized()
{
    auto r = getLocalBounds();
    auto w = r.getWidth();
    auto lw = w / 2;

    r.removeFromTop(20);
    auto btnRec = r.removeFromTop(50);
    btnRec.removeFromLeft(20);
    btn.setBounds(btnRec.removeFromLeft(50));

    r = getLocalBounds();
    r.removeFromTop(25);

    auto labels = r.removeFromTop(45);
    labels.removeFromLeft(w/3).reduced(5);

    auto labelsR = labels.removeFromRight(w/3);

    f1.setBounds(labels.removeFromTop(20));
    f2.setBounds(labels);
    intv.setBounds(labelsR.removeFromTop(20));
    diff.setBounds(labelsR);

    if(showOpts) {
        adsc.setVisible(true);
        r.removeFromTop(20);
        adsc.setBounds(r.removeFromTop(160));

        r.removeFromLeft(20);
        mode.setVisible(true);
        auto btns = r.removeFromTop(20);
        mode.setBounds(btns.removeFromLeft(100));

        //r.removeFromLeft(w/2);
        step.setVisible(true);
        step.setBounds(btns.removeFromLeft(200));

    } else {
        adsc.setVisible(false);
        mode.setVisible(false);
        step.setVisible(false);
        r.removeFromTop(20);
    }

    r.removeFromTop(20);
    auto sliders = r.removeFromLeft(w/2).reduced(20, 0);
    base.setBounds(sliders.removeFromBottom(40).reduced(0, 10));
 //   sliders.removeFromTop(20);
    frequencySlider1.setBounds(sliders);

    auto slidersR = r.removeFromLeft(w/2).reduced(20, 0);
    increment.setBounds(slidersR.removeFromBottom(40).reduced(0, 10));
   // slidersR.removeFromTop(20);
    interval.setBounds (slidersR);

}
